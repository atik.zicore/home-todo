import { useEffect, useState } from "react";

const TodoPractice = () => {
  const [tasks, setTasks] = useState([]);
  const [description, setDescriptions] = useState([]);
  const [inputTask, setInputTask] = useState("");
  const [inputDescription, setInputDescription] = useState("");
  const [editIndex, setEditIndex] = useState();
  const [editedTask, setEditedTask] = useState();
  const [editedDescription, setEditedDescription] = useState("");
 const [search, setSearch] = useState("")
 
  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (inputTask.trim() !== "") {
      const newTasks = [...tasks, inputTask];
      const newDescriptions = [...description, inputDescription];
      setTasks(newTasks);
      setDescriptions(newDescriptions);
      localStorage.setItem("tasks", JSON.stringify(newTasks));
      localStorage.setItem("descriptions", JSON.stringify(newDescriptions));
      setInputTask("");
      setInputDescription("");
    } else {
      alert("Cannot add empty value");
    }
  };

  useEffect(() => {
    const savedTasks = JSON.parse(localStorage.getItem("tasks")) || [];
    const savedDescriptions =
      JSON.parse(localStorage.getItem("descriptions")) || [];
    setTasks(savedTasks);
    setDescriptions(savedDescriptions);
  }, []);

  const deleteTask = (index) => {
    const updatedTasks = tasks.filter((task, i) => i !== index);
    const updatedDescriptions = description.filter(
      (descValue, i) => i !== index
    );
    setTasks(updatedTasks);
    setDescriptions(updatedDescriptions);
    localStorage.setItem("tasks", JSON.stringify(updatedTasks));
    localStorage.setItem("descriptions", JSON.stringify(updatedDescriptions));
  };

  // Delete functionallity one by splice
  // const deleteTask = (index) => {
  //     const removeTask = [...tasks];
  //     const removeDesc = [...description];
  //     removeTask.splice(index,1)
  //     removeDesc.splice(index,1)
  //     setTasks(removeTask);
  //     setDescriptions(removeDesc);
  // }

  // Delete functionallity two by filter
  // const deleteTask = (index) => {
  //     // (task, i) => i !== index: This is the callback function provided to filter. For each element (task) in the tasks array, along with its index (i), it checks if the index i is not equal (!==) to the index passed to the deleteTask function. If it's not equal, it means that the current task is not the one we want to delete, so it keeps that task in the new array updatedTasks.
  //     const updatedTasks = tasks.filter((task,i) => i !== index);
  //     const updatedDescriptions = description.filter((descValue, i) => i !== index);
  //     setDescriptions(updatedDescriptions)
  //     setTasks(updatedTasks);
  // }

  // edittask
  const editTask = (index) => {
    setEditIndex(index);
    setEditedTask(tasks[index]);
    setEditedDescription(description[index]);
  };

  const handleFormUpdate = (e) => {
    e.preventDefault();
    const updatedTask = [...tasks];
    const updatedDescriptions = [...description];
    updatedTask[editIndex] = editedTask;
    updatedDescriptions[editIndex] = editedDescription;
    setTasks(updatedTask);
    setDescriptions(updatedDescriptions);
    localStorage.setItem("tasks", JSON.stringify(updatedTask));
    localStorage.setItem("descriptions", JSON.stringify(updatedDescriptions));
    setEditIndex("");
    //  setEditedTask("");
    //  setEditedDescription("")
  };

  return (
    <div className="w-full h-auto bg-[#34495e]">
      <div className="w-full md:w-1/2 lg:w-3/4 h-full mx-auto">
        <h2 className="capitalize text-base md:text-lg lg:text-xl font-bold text-white py-4 md:py-8">
          todo list
        </h2>
        {/* form */}
        <div className="w-full h-full mx-auto ">
          <form
            className="flex justify-center items-center gap-x-4"
            onSubmit={handleFormSubmit}
          >
            <div className="w-full md:basis-1/4  flex flex-col space-y-2">
              <label
                htmlFor="taskName"
                className="capitalize font-bold text-white"
              >
                Title
              </label>
              <input
                type="text"
                name="taskName"
                id="taskName"
                className="w-full border border-black/30 p-1 outline-none focus:outline-emerald-400 rounded-lg placeholder:capitalize placeholder:font-bold"
                placeholder="enter task"
                onChange={(e) => setInputTask(e.target.value)}
                value={inputTask}
              />
            </div>
            <div className="w-full md:basis-1/4  flex flex-col space-y-2">
              <label
                htmlFor="taskDescription"
                className="capitalize font-bold text-white"
              >
                Description
              </label>
              <input
                type="text"
                name="taskDescription"
                id="taskDescription"
                className="w-full border border-black/30 p-1 outline-none focus:outline-emerald-400 rounded-lg placeholder:capitalize placeholder:font-bold"
                placeholder="enter description"
                onChange={(e) => setInputDescription(e.target.value)}
                value={inputDescription}
              />
            </div>
            <div className="w-full md:basis-1/4  flex flex-col items-center justify-center space-y-2">
              <label htmlFor=""> </label>
              <input
                type="submit"
                className="bg-emerald-400 px-4 py-2 rounded-lg uppercase"
                value="Add"
              />
            </div>
            <div className="w-full md:basis-1/4  flex flex-col items-center justify-center space-y-2">
              <label htmlFor="" className=" font-bold text-white">
                Search
              </label>
              <input
                type="search"
                className="w-full border border-black/30 p-1 outline-none focus:outline-emerald-400 rounded-lg placeholder:capitalize placeholder:font-bold"
                placeholder="search"
                onChange={(e) =>setSearch(e.target.value)}
                value={search}
              />
            </div>
          </form>
          <section className="w-3/4 h-auto py-8 mx-auto my-20 px-8 bg-[#2c3e50]">
            <ul className="space-y-2 ">
              {tasks.filter((item, index) =>item.toLowerCase().includes(search.toLowerCase())).map((todoItem, index) => {
                return (
                  <li
                    className="flex justify-between items-center even:bg-gray-700 p-4"
                    key={index}
                  >
                    {editIndex === index ? (
                      <div>
                        <form onSubmit={handleFormUpdate}>
                          <input
                            type="text"
                            placeholder="Task"
                            className="p-2 border border-black"
                            onChange={(e) => setEditedTask(e.target.value)}
                            value={editedTask}
                          />
                          <input
                            type="text"
                            placeholder="Task"
                            className="p-2 border border-black"
                            onChange={(e) =>
                              setEditedDescription(e.target.value)
                            }
                            value={editedDescription}
                          />
                          <input
                            type="submit"
                            value="update"
                            className="text-sm px-5 py-2 rounded-lg bg-emerald-600"
                          />
                        </form>
                      </div>
                    ) : (
                      <div>
                        <div className="basis-2/3 flex justify-start items-center gap-x-8">
                          <p className="text-white text-lg font-bold">
                            {index + 1}.{" "}
                          </p>
                          <div className="">
                            <p className=" text-white font-bold capitalize">
                              {todoItem}
                            </p>
                            <p className="text-white/70 capitalize">
                              {description[index]}
                            </p>
                          </div>
                        </div>
                        <div className="basis-1/3 flex gap-4 justify-center items-center">
                          <button
                            type="button"
                            className="px-4 py-2 capitalize text-base text-emerald-300  "
                          >
                            complete
                          </button>
                          <button
                            type="button"
                            className="px-4 py-2 capitalize text-base "
                            onClick={() => editTask(index)}
                          >
                            edit
                          </button>
                          <button
                            type="button"
                            className="px-4 py-2 capitalize text-base text-red-500 "
                            onClick={() => deleteTask(index)}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    )}
                  </li>
                );
              })}
            </ul>
          </section>
        </div>
      </div>
    </div>
  );
};

export default TodoPractice;
