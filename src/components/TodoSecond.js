import { useState } from "react";

const TodoSecond = () => {
  const [tasks, setTasks] = useState([]);
  const [inputValue, setInputValue] = useState("");
  // edit

  const [editIndex, setEditIndex] = useState();
  const [editValue, setEditValue] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const updatedTasks = [...tasks, inputValue];
    setTasks(updatedTasks);
    setInputValue("");
    console.log("tasks", updatedTasks);
  };

  // Delete
  const deleteTask = (index) => {
    const removeTask = [...tasks];
    removeTask.splice(index, 1);
    setTasks(removeTask);
  };

  //Edit

  const editTask = (index) => {
    setEditIndex(index);
    setEditValue(tasks[index]);
  };

  //updart
  const handleFormUpdate = () => {
    const updateValue = [...tasks];
    updateValue[editIndex] = editValue;
    setTasks(updateValue);
    setEditIndex("");
    setEditValue("");
  };

  return (
    <div>
      <h2>todo second</h2>
      <form
        className="w-2/3 mx-auto flex gap-x-4 justify-center pt-8"
        onSubmit={handleFormSubmit}
      >
        <input
          type="text"
          placeholder="Enter Task"
          name="task"
          className="w-1/3 border border-black"
          onChange={(e) => setInputValue(e.target.value)}
          value={inputValue}
        />
        <input type="submit" className="bg-emerald-400 p-4" />
      </form>
    
      <div>
        <ul className="space-y-3">
          {tasks.map((todoItem, index) => {
            return (
              <li key={index} className="flex justify-center">
                {editIndex === index ? (
                  <form
                    className="w-2/3 mx-auto flex gap-x-4 justify-center pt-8"
                    onSubmit={handleFormUpdate}
                  >
                    <input
                      type="text"
                      placeholder="Enter Task"
                      name="task"
                      className="w-1/3 border border-black"
                      onChange={(e) => setEditValue(e.target.value)}
                      value={editValue}
                    />
                    <input type="submit" className="bg-emerald-400 p-4" />
                  </form>
                ) : (
                  <div>
                    {" "}
                    <p>{index + 1}. </p>
                    <p>{todoItem}</p>
                    <div className="flex gap-4 ml-8">
                      <button type="button" onClick={() => editTask(index)}>
                        edit
                      </button>
                      <button type="button" onClick={() => deleteTask(index)}>
                        delete
                      </button>
                    </div>
                  </div>
                )}
              </li>
            );
          })}
        </ul>
      </div>
     

    </div>
  );
};

export default TodoSecond;
