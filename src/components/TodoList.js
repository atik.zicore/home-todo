import React, { useEffect, useState } from "react";

const TodoList = () => {
  const [tasks, setTasks] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [editIndex, setEditIndex] = useState(); //can use both coz its initializing index
  // const [editIndex, setEditIndex] = useState(-1);
  const [editValue, setEditValue] = useState("");
  const [inputDescription, setInputDescription] = useState("");
  const [description, setDescription] = useState("");



  const [search, setSearch] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault(); // Prevents the default form submission behavior, which would cause the page to reload

    if (inputValue.trim() !== "") {
      // Creates a new array containing all the existing tasks (spread operator),
      // and adds the new inputValue  to the end of the array.
      const updatedTasks = [...tasks, inputValue];
      setTasks(updatedTasks);
      localStorage.setItem("task", JSON.stringify(updatedTasks));
      setInputValue("");
    } else {
      alert("Cannot add empty value");
    }
  };

  useEffect(() => {
    const savedTasks = JSON.parse(localStorage.getItem("task"));
    if (savedTasks) {
      setTasks(savedTasks);
    }
  }, []);

  //editUser: This is a function that gets triggered when the "edit" button is clicked for a particular task. It takes the index of the task as an argument, which is the position of the task in the tasks array.
  //setEditIndex(index): This line sets the editIndex state variable to the index of the task that is being edited. By setting editIndex to the index of the task, we indicate that this task is currently being edited. This will trigger the rendering logic to display the input field for editing this task.
  //setEditValue(tasks[index]): This line sets the editValue state variable to the value of the task at the given index. This value is used to populate the input field with the current value of the task being edited. By setting editValue, we ensure that the input field displays the current task content for editing.

  //=============== First Edit working
  // const editTask = (index) => {
  //   setEditIndex(index);
  //   setEditValue(tasks[index]);
  // };

  // second edit

  const editTask = (index) => {
    // Retrieve the task at the specified index
    const selectedTask = tasks[index];
    // Clone the tasks array
    const updatedTasks = [...tasks];
    // Set the selected task as the editValue
    setEditValue(selectedTask);
    // Update the tasks array with the clone
    setTasks(updatedTasks);
    // Set the editIndex to the specified index
    setEditIndex(index);
  };

  const handleUpdate = () => {
    const updatedTasks = [...tasks]; // Create a copy of the tasks array
    updatedTasks[editIndex] = editValue; // Update the task at the editIndex with the new editValue
    localStorage.setItem("task", JSON.stringify(updatedTasks));
    setTasks(updatedTasks); // Update the tasks array in the state with the updatedTasks
    setEditIndex(-1); // Reset the editIndex to -1, indicating that no task is being edited
    setEditValue(""); // Clear the editValue, resetting the input field
  };

  const handleCancel = () => {
    setEditIndex(-1);
    setEditValue("");
  };

  // Delete Task one Working
  // const deleteTask =(index) => {
  //   // In this function, it checks if the deletedIndex is not equal to the index passed to the deleteTask function. If they are not equal, it means the current element should be kept in the new array. If they are equal, it means the current element should be deleted.
  //   const newTask = tasks.filter((value,deletedIndex) => {return deletedIndex !== index})
  //   setTasks(newTask);
  // }

  // Delete Task two working
  const deleteTask = (index) => {
    const removeTask = [...tasks]; //create new array with previous data
    removeTask.splice(index, 1);
    localStorage.setItem("task", JSON.stringify(removeTask));
    setTasks(removeTask);
  };
  //   const filteredTasks = tasks.filter((task) =>
  //   task.toLowerCase().includes(search.toLowerCase())
  // );

  return (
    <div className="bg-[#34495e] h-screen w-full">
      <h2 className="font-bold text-lg">Todo list</h2>

      <div className="flex justify-center items-center gap-x-2 px-4 bg-teal-400">
        <form className=" py-6 px-4 w-2/3" onSubmit={handleFormSubmit}>
          <div className="flex gap-4 justify-center">
            <input
              type="text"
              placeholder="Enter Task"
              className="w-1/3 border outline-none border-black/20 rounded-lg p-2 text-black"
              onChange={(e) => setInputValue(e.target.value)}
              value={inputValue}
            />
            <input
              type="text"
              placeholder="Enter Description"
              className="w-1/3 border outline-none border-black/20 rounded-lg p-2 text-black"
              onChange={(e) => setInputDescription(e.target.value)}
              value={inputDescription}
            />
            <button type="submit" className="px-4 py-2 bg-green-500 w-1/3">
              {" "}
              Add task
            </button>
          </div>
        </form>
        <input
          type="search"
          className="px-4 py-2 border border-black outline-none"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>

      {/* //   const filteredTasks = tasks.filter((task) =>
//   task.toLowerCase().includes(search.toLowerCase())
// ); */}

      <div className="w-2/3  mx-auto py-4 lg:py-8">
        <ul>
          {tasks
            .filter((item) => item.toLowerCase().includes(search))
            .map((todoItem, index) => (
              <li className="flex justify-between text-white" key={index}>
                {editIndex === index ? (
                  <>
                    <input
                      type="text"
                      value={editValue}
                      onChange={(e) => setEditValue(e.target.value)}
                      className="w-2/3 border outline-none border-black/20 rounded-lg p-2 text-black"
                    />
                    <div className="flex gap-x-4">
                      <button
                        type="button"
                        className="capitalize text-base"
                        onClick={handleUpdate}
                      >
                        Update
                      </button>
                      <button
                        type="button"
                        className="capitalize text-base text-red-600"
                        onClick={handleCancel}
                      >
                        Cancel
                      </button>
                    </div>
                  </>
                ) : (
                  <div className="w-2/3 mx-auto even:bg-gray-500/40 flex justify-start gap-x-4 items-start text-start">
                  <p>{index + 1}. </p>
                  <p className="capitalize ">{todoItem}</p>
                    {/* <p>
                      {index + 1}
                      <span className="font-bold capitalize"> {todoItem}</span>
                    </p> */}
                    <div className="flex gap-x-4">
                      <button
                        type="button"
                        className="capitalize text-base"
                        onClick={() => editTask(index)}
                      >
                        Edit
                      </button>
                      <button
                        type="button"
                        className="capitalize text-base text-red-600"
                        onClick={() => deleteTask(index)}
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                )}
              </li>
            ))}
        </ul>
        {/* <ul>
          {tasks.map((todoItem, index) => (
            <li className="flex justify-between text-white" key={index}>
              {editIndex === index ? (
                <>
                  <input
                    type="text"
                    value={editValue}
                    onChange={(e) => setEditValue(e.target.value)}
                    className="w-2/3 border outline-none border-black/20 rounded-lg p-2 text-black"
                  />
                  <div className="flex gap-x-4">
                    <button
                      type="button"
                      className="capitalize text-base"
                      onClick={handleUpdate}
                    >
                      Update
                    </button>
                    <button
                      type="button"
                      className="capitalize text-base text-red-600"
                      onClick={handleCancel}
                    >
                      Cancel
                    </button>
                  </div>
                </>
              ) : (
                <>
                  <p>
                    {index + 1}
                    <span className="font-bold"> {todoItem}</span>
                  </p>
                  <div className="flex gap-x-4">
                    <button
                      type="button"
                      className="capitalize text-base"
                      onClick={() => editTask(index)}
                    >
                      Edit
                    </button>
                    <button
                      type="button"
                      className="capitalize text-base text-red-600"
                      onClick={() => deleteTask(index)}
                    >
                      Delete
                    </button>
                  </div>
                </>
              )}
            </li>
          ))}
        </ul> */}
      </div>

      {/* <div
        className="h-screen pt-96"
        data-role="imagemagnifier"
        data-magnifier-mode="glass"
        data-lens-type="circle"
        data-lens-size="200"
      >
        <img src="Images/one.jpeg" className="w-[30rem] h-[30rem]" />
      </div>
      <MagnifierContainer>
        <div className="example-class">
          <MagnifierPreview imageSrc="Images/one.jpeg" />
        </div>
        <MagnifierZoom style={{ height: "400px" }} imageSrc="Images/one.jpeg" />
      </MagnifierContainer> */}
    </div>
  );
};

export default TodoList;
