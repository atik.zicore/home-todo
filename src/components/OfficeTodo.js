import React, { useEffect, useState } from "react";
import { Switch } from "@headlessui/react";

const OfficeTodo = () => {
  const [allTask, setAllTask] = useState([]);
  const [allDescription, setAllDescription] = useState([]);
  const [inputTask, setInputTask] = useState("");
  const [inputDescription, setInputDescription] = useState("");
  const [editIndex, setEditIndex] = useState(null);
  const [editedTask, setEditedtask] = useState("");
  const [editedDescription, setEditedDescription] = useState("");
  // const [completedTasks, setCompletedTasks] = useState((false));
  const [completedTasks, setCompletedTasks] = useState(
    new Array(allTask.length).fill(false)
  );
  const [search, setSearch] = useState("");
  const [theme, setTheme] = useState(null);
  const [enabled, setEnabled] = useState(false);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const updatedTask = [...allTask, inputTask];
    const updatedDescription = [...allDescription, inputDescription];
    setAllTask(updatedTask);
    setAllDescription(updatedDescription);
    localStorage.setItem("allTask", JSON.stringify(updatedTask));
    localStorage.setItem("allDescription", JSON.stringify(updatedDescription));
    setInputTask("");
    setInputDescription("");
    // console.log(updatedTask, updatedDescription);
  };

  useEffect(() => {
    const savedTasks = JSON.parse(localStorage.getItem("allTask")) || [];
    const savedDescription =
      JSON.parse(localStorage.getItem("allDescription")) || [];
    setAllTask(savedTasks);
    setAllDescription(savedDescription);
    console.log("print");
  }, []);

  // // delete functionallity one
  // const deleteTask = (index) => {
  //   const removeTask = [...allTask];
  //   const removeDescription = [...allDescription];
  //   removeTask.splice(index, 1);
  //   removeDescription.splice(index, 1);
  //   setAllTask(removeTask);
  //   setAllDescription(removeDescription);
  // };

  // delete function two
  const deleteTask = (index) => {
    const updatedTask = allTask.filter((task, i) => i !== index);

    const updatedDescription = allDescription.filter(
      (description, i) => i !== index
    );
    localStorage.setItem("allTask", JSON.stringify(updatedTask));
    localStorage.setItem("allDescription", JSON.stringify(updatedDescription));
    setAllTask(updatedTask);
    setAllDescription(updatedDescription);
  };

  // edit functionality
  const editTask = (index) => {
    setEditIndex(index);
    setEditedDescription(allDescription[index]);
    setEditedtask(allTask[index]);
  };
  // Update
  const handleUpdateForm = (e) => {
    e.preventDefault();
    const updatedTask = [...allTask];
    const updatedDescription = [...allDescription];
    updatedTask[editIndex] = editedTask;
    updatedDescription[editIndex] = editedDescription;
    setAllDescription(updatedDescription);
    localStorage.setItem("allTask", JSON.stringify(updatedTask));
    localStorage.setItem("allDescription", JSON.stringify(updatedDescription));
    setAllTask(updatedTask);
    setEditIndex("");
  };
  //   const completeTask = (index) => {
  //     const updatedCompletedTasks = [...completedTasks];
  //     updatedCompletedTasks[index] = !updatedCompletedTasks[index];
  //     setCompletedTasks(updatedCompletedTasks);
  // };

  const completeTask = (index) => {
    const updatedCompletedTasks = [...completedTasks];
    updatedCompletedTasks[index] = !updatedCompletedTasks[index];
    setCompletedTasks(updatedCompletedTasks);
  };

  const handleThemeColor = () => {
    setTheme(theme === "dark" ? "light" : "dark");
  };
  useEffect(() => {
    if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  }, []);
  // useEffect(() => {
  //   // if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
  //   if (theme === "dark") {
  //     document.documentElement.classList.add("dark");
  //   } else {
  //     document.documentElement.classList.remove("dark");
  //   }
  // }, [theme]);

  return (
    <div className="w-full h-full bg-gray-500/40 ">
      <div>
        <h1 className="text-lg capitalize font-bold text-center">
          office todo list
        </h1>
        {/* <button
          type="button"
          className="text-xs capitalize"
          onClick={handleThemeColor}
        >
          {theme === "dark" ? "light" : "dark"}
        </button> */}
      </div>

      <section className="w-2/3 mx-auto h-full py-4 lg:py-12 dark:bg-blue-800/30">
        <form
          className=" flex gap-4 justify-center"
          onSubmit={handleFormSubmit}
        >
          <div className="flex gap-4 flex-col justify-start items-start">
            <label htmlFor="" className="capitalize font-bold">
              task title
            </label>
            <input
              type="text"
              name="task"
              placeholder="Enter task title"
              className="outline-none p-2 border border-black "
              onChange={(e) => setInputTask(e.target.value)}
              value={inputTask}
            />
          </div>
          <div className="flex gap-4 flex-col justify-start items-start">
            <label htmlFor="description" className="capitalize font-bold">
              task description
            </label>
            <input
              type="text"
              name="description"
              placeholder="Enter task description"
              id="description"
              className="outline-none p-2 border border-black "
              onChange={(e) => setInputDescription(e.target.value)}
              value={inputDescription}
            />
          </div>
          <div className="flex gap-4 flex-col justify-start items-start">
            <label
              htmlFor="decription"
              className="capitalize font-bold"
            ></label>
            <input
              type="submit"
              value="Add task"
              name=""
              placeholder="Enter task description"
              id=""
              className="p-4 bg-emerald-400 "
            />
          </div>
          <div className="flex gap-4 flex-col justify-start items-start">
            <label htmlFor="search" className="capitalize font-bold">
              Search
            </label>
            <input
              type="search"
              name="search"
              placeholder="Enter task search"
              id="search"
              className="outline-none p-2 border border-black "
              onChange={(e) => setSearch(e.target.value)}
              value={search}
            />
          </div>
        </form>
      </section>
      {/* <section className="w-2/3 mx-auto h-full px-8">
        <ul>
          {allTask
            .filter((item, index) =>
              item.toLowerCase().includes(search.toLowerCase())
            )
            .map((todoItem, index) => {
              return (
                <li className="" key={index}>
                  {editIndex === index ? (
                    <div>
                      <form onSubmit={handleUpdateForm}>
                        <input
                          type="text "
                          placeholder="Edit"
                          onChange={(e) => setEditedtask(e.target.value)}
                          value={editedTask}
                        />
                        <input
                          type="text "
                          placeholder="Edit"
                          onChange={(e) => setEditedDescription(e.target.value)}
                          value={editedDescription}
                        />
                        <input type="submit" value="update" />
                      </form>
                    </div>
                  ) : (
                    <div className="flex justify-between">
                      <div className="flex ">
                        <p> {index + 1} </p>
                        <div className="px-2">
                          <p
                            className={
                              completedTasks[index] ? "line-through" : ""
                            }
                          >
                            {todoItem}
                          </p>
                          <p>{allDescription[index]}</p>
                        </div>
                      </div>
                      <div className="flex gap-4 ">
                        <button
                          type="button"
                          className="text-sm  capitalize"
                          onClick={() => completeTask(index)}
                        >
                          complete
                        </button>
                        <button
                          type="button"
                          className="text-sm  capitalize"
                          onClick={() => editTask(index)}
                        >
                          edit
                        </button>
                        <button
                          type="button"
                          className="text-sm  capitalize"
                          onClick={() => deleteTask(index)}
                        >
                          delete
                        </button>
                      </div>
                    </div>
                  )}
                </li>
              );
            })}
        </ul>
      </section> */}
      <section className="w-2/3 mx-auto h-full px-8">
        <ul>
          {allTask
            .filter((item, index) =>
              item.toLowerCase().includes(search.toLowerCase())
            )
            .map((todoItem, filteredIndex) => {
              // Get the original index by finding the index in the original array
              // const originalIndex = allTask.findIndex(
              //   (item) => item.toLowerCase() === todoItem.toLowerCase()
              // );

              // // Incremented count for visible items
              // const visibleIndex = originalIndex + 1;

              const originalIndex = allTask.findIndex(
                (item) => item.toLowerCase() === todoItem.toLowerCase()
              );
              const updatedIndex = originalIndex + 1;

              return (
                <li className="" key={filteredIndex}>
                  {editIndex === originalIndex ? (
                    <div>
                      <form onSubmit={handleUpdateForm}>
                        <input
                          type="text"
                          placeholder="Edit"
                          onChange={(e) => setEditedtask(e.target.value)}
                          value={editedTask}
                        />
                        <input
                          type="text"
                          placeholder="Edit"
                          onChange={(e) => setEditedDescription(e.target.value)}
                          value={editedDescription}
                        />
                        <input type="submit" value="update" />
                      </form>
                    </div>
                  ) : (
                    <div className="flex justify-between">
                      <div className="flex ">
                        <p> {updatedIndex} </p>
                        <div className="px-2">
                          <p
                            className={
                              completedTasks[originalIndex]
                                ? "line-through"
                                : ""
                            }
                          >
                            {todoItem}
                          </p>
                          <p>{allDescription[originalIndex]}</p>
                        </div>
                      </div>
                      <div className="flex gap-4">
                        <button
                          type="button"
                          className="text-sm capitalize"
                          onClick={() => completeTask(originalIndex)}
                        >
                          complete
                        </button>
                        <button
                          type="button"
                          className="text-sm capitalize"
                          onClick={() => editTask(originalIndex)}
                        >
                          edit
                        </button>
                        <button
                          type="button"
                          className="text-sm capitalize"
                          onClick={() => deleteTask(originalIndex)}
                        >
                          delete
                        </button>
                      </div>
                    </div>
                  )}
                </li>
              );
            })}
        </ul>
      </section>
      <section className="w-full h-screen bg-gray-600">
        <Switch
          checked={enabled}
          onChange={setEnabled}
          className={`${
            enabled ? "bg-red-600" : "bg-gray-200"
          } relative inline-flex h-6 w-11 items-center rounded-full`}
        >
          <span className="sr-only">Enable notifications</span>
          <span
            className={`${
              enabled ? "translate-x-6" : "translate-x-1"
            } inline-block h-4 w-4 transform rounded-full bg-white transition`}
          />
        </Switch>
        <div className=""></div>
      </section>
    </div>
  );
};

export default OfficeTodo;
