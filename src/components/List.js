import React from 'react'

const List = ({items, removeItems, editItems}) => {
  return (
    <div className='w-full'>
        <h2>hi</h2>
        {items.map((item) => {
            const {id, title} = item;
            return (
                <ul>
                    <li>{title}
                    <div>
                        <button className='px-4 border border-black/50' onClick={() => editItems(id)}>edit</button>
                        <button>delete</button>
                    </div>
                    </li>
                </ul>
            )
        })}
    </div>
  )
}

export default List