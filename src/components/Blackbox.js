import React, { useEffect, useState } from "react";

const TodoList = () => {
  const [tasks, setTasks] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [editIndex, setEditIndex] = useState();
  const [editValue, setEditValue] = useState("");
  const [inputDescription, setInputDescription] = useState("");
  const [descriptions, setDescriptions] = useState([]);
  const [search, setSearch] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (inputValue.trim() !== "") {
      setTasks([...tasks, inputValue]);
      setDescriptions([...descriptions, inputDescription]);
      localStorage.setItem("task", JSON.stringify([...tasks, inputValue]));
      localStorage.setItem("description", JSON.stringify([...descriptions, inputDescription]));
      setInputValue("");
      setInputDescription("");
    } else {
      alert("Cannot add empty value");
    }
  };
  


  // const handleFormSubmit = (e) => {
  //   e.preventDefault();
  //   if (inputValue.trim() !== "") {
  //     setTasks((prevTasks) => {
  //       const updatedTasks = [...prevTasks, inputValue];
  //       localStorage.setItem("task", JSON.stringify(updatedTasks));
  //       return updatedTasks;
  //     });
  //     setDescriptions((prevDescriptions) => {
  //       const updatedDescriptions = [...prevDescriptions, inputDescription];
  //       localStorage.setItem("description", JSON.stringify(updatedDescriptions));
  //       return updatedDescriptions;
  //     });
  //     setInputValue("");
  //     setInputDescription("");
  //   } else {
  //     alert("Cannot add empty value");
  //   }
  // };
  




  // const handleFormSubmit = (e) => {
  //   e.preventDefault();
  //   if (inputValue.trim() !== "") {
  //     const updatedTasks = [...tasks, inputValue];
  //     const updatedDescriptions = [...descriptions, inputDescription];
  //     setTasks(updatedTasks);
  //     setDescriptions(updatedDescriptions);
  //     localStorage.setItem("task", JSON.stringify(updatedTasks));
  //     localStorage.setItem("description", JSON.stringify(updatedDescriptions));
  //     setInputValue("");
  //     setInputDescription("");
  //     // console.log("loaded")
  //   } else {
  //     alert("Cannot add empty value");
  //   }
  // };

  useEffect(() => {
    const savedTasks = JSON.parse(localStorage.getItem("task")) || [];
    const savedDescriptions = JSON.parse(localStorage.getItem("description")) || [];
    setTasks(savedTasks);
    setDescriptions(savedDescriptions);
  }, []);

  const editTask = (index) => {
    const selectedTask = tasks[index];
    const selectedDescription = descriptions[index];
    setEditValue(selectedTask);
    setInputDescription(selectedDescription);
    setEditIndex(index);
  };

  const handleUpdate = () => {
    const updatedTasks = [...tasks];
    const updatedDescriptions = [...descriptions];
    updatedTasks[editIndex] = editValue;
    updatedDescriptions[editIndex] = inputDescription;
    localStorage.setItem("task", JSON.stringify(updatedTasks));
    localStorage.setItem("description", JSON.stringify(updatedDescriptions));
    setTasks(updatedTasks);
    setDescriptions(updatedDescriptions);
    setEditIndex(-1);
    setEditValue("");
    setInputDescription("");
  };

  const handleCancel = () => {
    setEditIndex(-1);
    setEditValue("");
    setInputDescription("");
  };

  const deleteTask = (index) => {
    const updatedTasks = [...tasks];
    const updatedDescriptions = [...descriptions];
    updatedTasks.splice(index, 1);
    updatedDescriptions.splice(index, 1);
    localStorage.setItem("task", JSON.stringify(updatedTasks));
    localStorage.setItem("description", JSON.stringify(updatedDescriptions));
    setTasks(updatedTasks);
    setDescriptions(updatedDescriptions);
  };

  return (
    <div className="bg-[#34495e] h-screen w-full">
      <h2 className="font-bold text-lg">Todo list</h2>

      <div className="flex justify-center items-center gap-x-2 px-4 bg-teal-400">
        <form className=" py-6 px-4 w-2/3" onSubmit={handleFormSubmit}>
          <div className="flex gap-4 justify-center">
            <input
              type="text"
              placeholder="Enter Task"
              className="w-1/3 border outline-none border-black/20 rounded-lg p-2 text-black"
              onChange={(e) => setInputValue(e.target.value)}
              value={inputValue}
            />
            <input
              type="text"
              placeholder="Enter Description"
              className="w-1/3 border outline-none border-black/20 rounded-lg p-2 text-black"
              onChange={(e) => setInputDescription(e.target.value)}
              value={inputDescription}
            />
            <button type="submit" className="px-4 py-2 bg-green-500 w-1/3">
              {" "}
              Add task
            </button>
          </div>
        </form>
        <input
          type="search"
          className="px-4 py-2 border border-black outline-none"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>

      <div className="w-2/3  mx-auto py-4 lg:py-8">
        <ul>
          {tasks
            .filter((item, index) => item.toLowerCase().includes(search.toLowerCase()))
            .map((todoItem, index) => (
              <li className="flex justify-between text-white" key={index}>
                {editIndex === index ? (
                  <>
                    <input
                      type="text"
                      value={editValue}
                      onChange={(e) => setEditValue(e.target.value)}
                      className="w-2/3 border outline-none border-black/20 rounded-lg p-2 text-black"
                    />
                    <input
                      type="text"
                      value={inputDescription}
                      onChange={(e) => setInputDescription(e.target.value)}
                      className="w-2/3 border outline-none border-black/20 rounded-lg p-2 text-black"
                    />
                    <div className="flex gap-x-4">
                      <button
                        type="button"
                        className="capitalize text-base"
                        onClick={handleUpdate}
                      >
                        Update
                      </button>
                      <button
                        type="button"
                        className="capitalize text-base text-red-600"
                        onClick={handleCancel}
                      >
                        Cancel
                      </button>
                    </div>
                  </>
                ) : (
                  <div className="w-2/3 mx-auto even:bg-gray-500/40 flex justify-start gap-x-4 items-start text-start">
                    <p>{index + 1}. </p>
                    <p className="capitalize">{todoItem}</p>
                    <p>{descriptions[index]}</p>
                    <div className="flex gap-x-4">
                      <button
                        type="button"
                        className="capitalize text-base"
                        onClick={() => editTask(index)}
                      >
                        Edit
                      </button>
                      <button
                        type="button"
                        className="capitalize text-base text-red-600"
                        onClick={() => deleteTask(index)}
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                )}
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default TodoList;
