import React from "react";
import ReactDOM from "react-dom";
import { Magnifier } from "react-image-magnifiers";
import ReactImageZoom from "react-image-zoom";
import InnerImageZoom from 'react-inner-image-zoom';

const ImageMagnify = () => {
  const props = {
    width: 400,
    height: 250,
    zoomWidth: 500,
    img: "./Images/one.jpeg",
  };
  return (
    <div className="w-full h-screen">
      <img src="./Images/one.jpeg" alt="" className="w-full h-full" />

      {/* <div>
      <ReactImageZoom {...props} />
      </div> */}

      {/* <InnerImageZoom src="./Images/one.jpeg" zoomScale={1.5} width={400} height={400} /> */}

      <Magnifier src="./Images/one.jpeg" width={500} zoomImgSrc="./Images/one.jpeg" zoomFactor={1.5} mgWidth={150} mgHeight={150} mgBorderWidth={2} mgShape="circle" mgMouseOffsetX={0} mgMouseOffsetY={0} mgTouchOffsetX={-50} mgTouchOffsetY={-50}/>;

      {/* <ReactImageMagnify
        {...{
          smallImage: {
            alt: "Wristwatch by Ted Baker London",
            isFluidWidth: true,
            src: watchImg300,
          },
          largeImage: {
            src: watchImg1200,
            width: 1200,
            height: 1800,
          },
        }}
      /> */}

      <div></div>
    </div>
  );
};

export default ImageMagnify;
