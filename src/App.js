import React from 'react'
import TodoList from './components/TodoList'
import './index.css'
import BlackBox from './components/Blackbox'
import TodoPractice from './components/TodoPractice'
import OfficeTodo from './components/OfficeTodo'
import ShowPass from './components/ShowPasswoed'
// import TodoSecond from './components/TodoSecond'
// import ImageMagnify from './components/ImageMagnify'

const App = () => {
  return (
    <div className='App dark:bg-black'>
      {/* <TodoList/> */}
      {/* <TodoSecond/> */}
      {/* <ImageMagnify/> */}
      {/* <BlackBox/> */}
      {/* <TodoPractice/> */}
      <OfficeTodo/>
      <ShowPass/>
    </div>
  )
}

export default App